"""Bài tập về nhà: Hãy sử dụng câu lệnh cmd 
để xóa my go ram ra khỏi u ni ve. Sau đó tạo 1 file gồ rem . 
py. Và hãy viết 1 chương trình python để hiện thì cm, với các yêu cầu sau: 
 s = "Today is nice day, so we can go anywhere"
 a/ hãy đếm số ký tự có trong chuỗi đã cho
 b/ Hãy xuất ký tự từ vị trí -5 đến -9
 c/ Xuất tất cả các ký tự trong chuỗi với bước nhảy là 3
 d/ Hãy tách chuỗi đã cho thành 2 chuỗi mới, s1 xuất "Today is nice day",
 s2 xuất "so we can go anywhere
 e/Hãy hợp nhất chuỗi s1 và s2 lại và xắp theo ký tự bảng chữ cái
 f/ Chuyển chuỗi sau khi hợp nhất thành chuỗi ký tự in hoa
 
 Hạn chót: 3h PM ngày thứ tư. Nộp ở : tới ngày đó thầy sẽ post cái gì đó. 
 Mình chỉ cần share đường dẫn. Upload lên gitlab. Không sử dụng colab
để tới thứ 5 học tiếp"""

a ="Today is a nice day , So we can go anywhere"
print("Chuỗi đã cho: ",a)
print("a/ Số ký tự trong chuỗi: ",len(a))
print("b/ Xuất từ vị trí -5 đến -9",a[-5:-9])
print("c/ Xuất toàn bộ chuỗi bước nhảy với bước nhảy là 3:",a[::3])

b = a[:19]
print('d/Chuỗi S1 là: ',b)
c = a[22:]
print("d/ Chuỗi S2 là: ",c)
s = ''.join([b, c])

print("e/ Chuỗi sau khi được hợp nhất và sắp xếp a-z",sorted(s))
print("f/Chuỗi sau khi được hợp nhất và in hoa: ",s.upper())
