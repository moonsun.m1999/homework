#ex_1
import math
def my_sqrt(a):
    x = a / 2
    while True:
        y = (x + a/x) / 2.0
        if y == x:
            break
        x = y
    return x

print(my_sqrt(12))

#ex_2
import math
def my_sqrt(a):
    x = a / 2
    while True:
        y = (x + a/x) / 2.0
        if y == x:
            break
        x = y
    return x

def test_sqrt():
    a = 0
    while (a < 25):
        a = a + 1
        my_sqrt(a)
        math.sqrt(a)
        diff = abs(math.sqrt(a) - my_sqrt(a))
        print("a =", a, "| my_sqrt(a) =", my_sqrt(a), "| math.sqrt =", math.sqrt(a), "| diff =", diff)
    return

test_sqrt()

#ex_3
prefixes = 'JKLMNOPQ'
suffix = 'ack'
for letter in prefixes:
     print(letter + suffix)

"ex_3-2"
prefixes = 'JKLMNOPQ'
suffix = 'ack'
for letter in prefixes:
    if letter not in prefixes[5::2]:
        print(letter + suffix)
    else:
        print(letter + 'u' + suffix)