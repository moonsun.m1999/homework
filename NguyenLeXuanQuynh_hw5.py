#ex1
def print_volume(x):
    v = 4/(3*3.1415926535897932*x*3)
    print(v)

print_volume(4) #0.0353677651315323
print_volume(5) #0.028294212105225845
print_volume(6) #0.0235785100876882

#ex 2
def tamgiac(a,b,c):
    d = a+b
    if c > d:
        print("Đây không phải tam giác")
    elif c < d:
        if a == b and a!= c and b!= c:
            print("Đây là tam giác cân")
        elif a == b and b == c:
            print("Đây là tam giác đều")
    else:
        print("Đây là tam giác bình thường")

tamgiac(12,12,12) #tam giác cân
tamgiac(0,0,4)    #không phải tam giác
tamgiac(12,12,3)  #tam giác đều
tamgiac(1,2,3)   #tam giác bình thường