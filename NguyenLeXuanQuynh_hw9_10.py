"""Part 1
Write a Python program that does the following. 
Create a string that is a long series of words separated by spaces. The string is your own creative choice. 
It can be names, favorite foods, animals, anything. Just make it up yourself. Do not copy the string 
from another source. 

Turn the string into a list of words using split. 

Delete three words from the list, but delete each one using a different kind of Python operation. 

Sort the list. 

Add new words to the list (three or more) using three different kinds of Python operation. 

Turn the list of words back into a single string using join. 

Print the string. """

sentence = "I need coffee, i haven't had enough sleep for week. Give me coffee, now!!"
lis = []
lis = sentence.split()
#print(lis)

#del_1
print(lis[1:])

#del_2
lis.pop(4)
print(lis)

#del_3
del lis[6]
print(lis)


#lis.sort()
#print(lis)


#ver_1
lis2 = ['COFFEE']
lis3 = lis + lis2
print(lis3)

#ver_2
lis3.append('Hello?')
print(lis3)

#ver_3
sentence2 = "I say give me coffee!!"
lis5 = []
lis5 = sentence2.split()
lis3.extend(lis5)
print(lis3)

space = " "
string2 = space.join(lis)
print(string2)


"""Part 2
Provide your own examples of the following using Python lists. Create your own examples. Do not copy 
them from another source. 
Nested lists 
The “*” operator 
List slices 
The “+=” operator 
A list filter 
A list operation that is legal but does the "wrong" thing, not what the programmer expects 
Provide the Python code and output for your program and all your examples. """

a = "Today is so hot"
lis = a.split()
print(lis)
print(lis*4)


lis2 = ["I", "need", "more", "ice"]
print (lis+lis2)


weather = ["hot", "windy", "cold"]
a = "Today is so hot"
lis = a.split()
def filter_weather(weather):
    b = lis[-1]
    if(b in weather):
        return True
    else:
        return False

filterweather = filter(filter_weather, weather)
print('The weather in list is:')
for lis in filterweather:
    print(lis)